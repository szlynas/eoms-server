package com.lynas.model.util

import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.AuthorityUtils
import org.springframework.security.core.userdetails.UserDetails

class SecurityUser(
        private val userName: String,
        private val userPassword: String = "",
        private val authorities: String) : UserDetails {

    override fun getAuthorities(): MutableCollection<out GrantedAuthority> {
        return AuthorityUtils.commaSeparatedStringToAuthorityList(authorities)
    }

    override fun isEnabled() = true

    override fun getUsername() = userName

    override fun isCredentialsNonExpired() = true

    override fun getPassword() = userPassword

    override fun isAccountNonExpired() = true

    override fun isAccountNonLocked() = true

}


