package com.lynas.config.security

import com.lynas.model.util.SecurityUser
import org.springframework.beans.factory.annotation.Value
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.authority.AuthorityUtils
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource
import org.springframework.stereotype.Component
import org.springframework.web.filter.OncePerRequestFilter
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class JwtAuthenticationTokenFilter(private val jwtTokenUtil: JWTTokenOps) : OncePerRequestFilter() {

    @Value("\${jwt.header}")
    private val tokenHeader: String? = null

    override fun doFilterInternal(request: HttpServletRequest, response: HttpServletResponse,
                                  filterChain: FilterChain) {


        val authToken = request.getHeader(this.tokenHeader)
        if (null != authToken) {
            val claims = jwtTokenUtil.getClaimsFromToken(authToken)
            if (null != claims) {
                if (jwtTokenUtil.isTokenExpired(claims) && SecurityContextHolder.getContext().authentication == null) {
                    val rolesString = jwtTokenUtil.getUserRole(claims)
                    val authorities = AuthorityUtils.commaSeparatedStringToAuthorityList(rolesString)
                    val authUser = SecurityUser(userName = claims.subject, authorities = rolesString)
                    val authentication = UsernamePasswordAuthenticationToken(authUser, null, authorities)
                    authentication.details = WebAuthenticationDetailsSource().buildDetails(request)
                    SecurityContextHolder.getContext().authentication = authentication
                }
            }
        }

        filterChain.doFilter(request, response)
    }
}
