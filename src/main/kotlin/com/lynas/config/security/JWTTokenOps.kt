package com.lynas.config.security

import com.lynas.model.Organization
import com.lynas.model.OrganizationInfo
import io.jsonwebtoken.Claims
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import org.springframework.beans.factory.annotation.Value
import org.springframework.mobile.device.Device
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.stereotype.Component
import java.util.*

@Component
class JWTTokenOps {

    private val CLAIM_KEY_USERNAME = "sub"
    private val CLAIM_KEY_ORG_ID = "org_id"
    private val CLAIM_KEY_ORG_NAME = "org_name"
    private val CLAIM_KEY_AUDIENCE = "audience"
    private val CLAIM_KEY_CREATED = "created"
    private val CLAIM_KEY_EXPIRED = "exp"
    private val CLAIM_KEY_ROLE = "rlu"

    private val AUDIENCE_UNKNOWN = "unknown"
    private val AUDIENCE_WEB = "web"
    private val AUDIENCE_MOBILE = "mobile"
    private val AUDIENCE_TABLET = "tablet"

    @Value("\${jwt.secret}")
    private val secret: String? = null

    @Value("\${jwt.expiration}")
    private val expiration: Long? = null


    private fun generateCurrentDate() = Date(System.currentTimeMillis())

    fun getClaimsFromToken(token: String?): Claims? = Jwts.parser().setSigningKey(secret).parseClaimsJws(token).body


    fun isTokenExpired(claims: Claims) = claims.expiration.before(generateCurrentDate())

    fun getUserRole(claims: Claims) = claims[CLAIM_KEY_ROLE] as String

    fun generateToken(userDetails: UserDetails, device: Device, organization: Organization): String {
        val now = Date().time
        val authorities = userDetails.authorities.map { it.authority }
        val claims: Map<String, Any> = mapOf(
                CLAIM_KEY_USERNAME to userDetails.username,
                CLAIM_KEY_ORG_ID to organization.id!!,
                CLAIM_KEY_ORG_NAME to organization.name,
                CLAIM_KEY_AUDIENCE to generateAudience(device),
                CLAIM_KEY_EXPIRED to now + expiration!!,
                CLAIM_KEY_CREATED to now,
                CLAIM_KEY_ROLE to authorities.joinToString(",")
        )
        return doGenerateToken(claims)
    }

    private fun doGenerateToken(claims: Map<String, Any>): String {
        return Jwts.builder()
                .setClaims(claims)
                .setExpiration(Date(Date().time + expiration!! * 1000))
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact()
    }


    private fun generateAudience(device: Device): String {
        var audience = AUDIENCE_UNKNOWN
        when {
            device.isNormal -> audience = AUDIENCE_WEB
            device.isTablet -> audience = AUDIENCE_TABLET
            device.isMobile -> audience = AUDIENCE_MOBILE
        }
        return audience
    }

    fun getOrganizationFromToken(token: String?): Organization {
        val claims = this.getClaimsFromToken(token)!!
        return Organization(claims[CLAIM_KEY_ORG_ID] as Long, claims[CLAIM_KEY_ORG_NAME] as String, 0,
                OrganizationInfo(null, "", ""))
    }


    fun getRefreshToken(token: String?): String {
        val claims = this.getClaimsFromToken(token) ?: throw IllegalStateException("Unable to parse claim from token")
        claims[CLAIM_KEY_CREATED] = generateCurrentDate()
        return doGenerateToken(claims)
    }

    fun isTokenRefreshable(token: String?, lastPasswordReset: Date): Boolean {
        val claims = this.getClaimsFromToken(token) ?: return false
        val dateCreated = Date(claims[CLAIM_KEY_CREATED] as Long)
        val createdBeforeLastPasswordReset = isCreatedBeforeLastPasswordReset(dateCreated, lastPasswordReset)
        val tokenExpired = isTokenExpired(claims)
        val ignoreTokenExpiration = ignoreTokenExpiration(claims)
        return (!createdBeforeLastPasswordReset && (!tokenExpired || ignoreTokenExpiration))
    }

    fun isCreatedBeforeLastPasswordReset(createDate: Date, lastPasswordResetDate: Date)
            = createDate.before(lastPasswordResetDate)

    fun ignoreTokenExpiration(claims: Claims)
            = (AUDIENCE_TABLET == claims[CLAIM_KEY_AUDIENCE] || AUDIENCE_MOBILE == claims[CLAIM_KEY_AUDIENCE])


}
